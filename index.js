var arrNumber = [];

arrNumbertest = [-3, -2, 5, 8, -2, -3, 9, 10, -8];
var nhonhat = timSoDuongNhoNhat(arrNumbertest);

// var arr1 = [1, 3, -2, -5];
function tongCacSoDuong(arr) {
  var sum = 0;
  arr.forEach(function (number) {
    if (number > 0) {
      sum += number;
    }
  });
  return sum;
}

function demCacSoDuong(arr) {
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      count++;
    }
  }
  return count;
}

function timSoNhoNhat(arr) {
  var min = arr[0];
  for (var i = 1; i < arr.length; i++) {
    if (arr[i] < min) {
      min = arr[i];
    }
  }
  return min;
}

function timSoDuongNhoNhat(arr) {
  var mangSoDuong = [];
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      mangSoDuong.push(arr[i]);
    }
  }
  return Math.min(...mangSoDuong);
}

function timSoChanCuoiCung(arr) {
  var num = -1;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] % 2 == 0) {
      num = arr[i];
    }
  }
  return num;
}

function sapXepTangDan(arr) {
  var min = arr[0];
  for (var i = 0; i < arr.length; i++) {
    for (var j = i + 1; j < arr.length; j++) {
      if (arr[i] > arr[j]) {
        min = arr[j];
        arr[j] = arr[i];
        arr[i] = min;
      }
    }
  }
  return arr;
}

function checkSoNguyenTo(n) {
  if (n < 2) {
    return false;
  }
  var canBacHai = Math.sqrt(n);
  for (var i = 2; i <= canBacHai; i++) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

function inSoNguyenToDauTien(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (checkSoNguyenTo(arr[i])) {
      break;
    }
  }
  return arr[i];
}

function demSoNguyen(arr) {
  var count = 0;

  for (var i = 0; i < arr.length; i++) {
    if (Number.isInteger(arr[i])) {
      count++;
    }
  }
  return count;
}

function demCacSoAm(arr) {
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] < 0) {
      count++;
    }
  }
  return count;
}

function hienThiKetQua() {
  if (document.querySelector("#txt-number").value.trim() == "") {
    return;
  }

  var soNhap = document.querySelector("#txt-number").value * 1;
  arrNumber.push(soNhap);

  document.querySelector("#txt-number").value = "";

  var tongSoDuong = tongCacSoDuong(arrNumber);
  var demSoDuong = demCacSoDuong(arrNumber);
  var soNhoNhat = timSoNhoNhat(arrNumber);
  var soDuongNhoNhat = timSoDuongNhoNhat(arrNumber);
  var soChanCuoiCung = timSoChanCuoiCung(arrNumber);
  var message = "";
  var demSoAm = demCacSoAm(arrNumber);
  var message2 = "";

  if (soChanCuoiCung == -1) {
    message = "Ko có số chẵn trong mảng";
  } else {
    message = "Số chẵn cuối cùng trong mảng " + soChanCuoiCung;
  }

  if (demSoDuong > demSoAm) {
    message2 = "Số dương > Số Âm";
  } else if (demSoDuong < demSoAm) {
    message2 = "Số dương < Số Âm";
  } else {
    message2 = "Số dương = Số Âm";
  }

  var soNguyenTo = inSoNguyenToDauTien(arrNumber);
  var tongCacSoNguyen = demSoNguyen(arrNumber);
  document.querySelector("#ketQua").innerHTML = /*html*/ `
    <div>
    <p> Mảng: ${arrNumber}</p>
    <p> Tổng các số dương: ${tongSoDuong}</p>
    <p> Có ${demSoDuong} số dương trong mảng</p>
    
    <p> Số nhỏ nhất trong mảng là ${soNhoNhat}</p>
    <p> Số dương nhỏ nhất trong mảng là ${soDuongNhoNhat}</p>
    <p> ${message}</p>
    <p> Mảng sắp xếp tăng dần : ${sapXepTangDan(arrNumber)}</p>
    <p> Số nguyên tố đầu tiên trong mảng: ${soNguyenTo}</p>
    <p> Có ${tongCacSoNguyen} số nguyên trong mảng</p>
    <p> ${message2}</p>
    </div>
  `;
}

function doiCho(arr, vt1, vt2) {
  var tam = 0;

  tam = arr[vt1];
  arr[vt1] = arr[vt2];
  arr[vt2] = tam;

  return arr;
}

function hienThiMangDoiCho() {
  if (
    document.querySelector("#txt-vitri1").value.trim() == "" ||
    document.querySelector("#txt-vitri2").value.trim() == ""
  ) {
    return;
  }
  var vt1 = document.querySelector("#txt-vitri1").value * 1;
  var vt2 = document.querySelector("#txt-vitri2").value * 1;

  document.querySelector("#txt-vitri1").value = "";
  document.querySelector("#txt-vitri2").value = "";
  var mangDoiCho = doiCho(arrNumber, vt1, vt2);
  document.querySelector("#ketQuaDoiCho").innerHTML = /*html*/ `
    <div>
    <p> Mảng đã đổi chỗ: ${mangDoiCho}</p>
    </div>
  `;
}
